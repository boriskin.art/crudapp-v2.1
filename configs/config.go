package configs

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

// InitConfig initialize configs from yml file via viper and set some from env
func InitConfig() {
	viper.AddConfigPath("./configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatal("Config parse error:", err)
	}

	// Update some value from env
	viper.Set("port", os.Getenv("CRUDAPP_PORT"))
	viper.Set("domain", os.Getenv("CRUDAPP_HOST"))

	viper.Set("jwt_secret", os.Getenv("CRUDAPP_JWT_SECRET"))

	// DB Settings
	viper.Set("db.host", os.Getenv("CRUDAPP_DB_HOST"))
	viper.Set("db.port", os.Getenv("CRUDAPP_DB_PORT"))
	viper.Set("db.dbname", os.Getenv("CRUDAPP_DB_DBNAME"))
	viper.Set("db.user", os.Getenv("CRUDAPP_DB_USER"))
	viper.Set("db.password", os.Getenv("CRUDAPP_DB_PASSWORD"))
	viper.Set("db.sslmode", os.Getenv("CRUDAPP_DB_SSLMODE"))
}
