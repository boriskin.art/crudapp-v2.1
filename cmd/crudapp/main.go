package main

import (
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/crudapp"
)

func main() {
	crudapp.Run()
}
