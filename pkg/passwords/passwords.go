package passwords

import (
	"crypto/sha1"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
)

// HashPassword pbkdf2
func HashPassword(salt, plainPassword string) string {
	hashedPass := pbkdf2.Key([]byte(plainPassword), []byte(salt), 4096, 32, sha1.New)
	return salt + fmt.Sprintf("%x", hashedPass)
}

// CheckPassword used to check user input password
func CheckPassword(passHash string, plainPassword string) bool {
	salt := passHash[0:16]
	userPassHash := HashPassword(fmt.Sprintf("%s", salt), plainPassword)
	return userPassHash == passHash
}