package logger

import (
	"github.com/sirupsen/logrus"
	"os"
)

// InitLogger initialize logger (logrus)
func InitLogger() {

	// Set formatter (for prod need to be JSON)
	// logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetFormatter(&logrus.TextFormatter{})

	// Log output (can be file (any io.Writer))
	logrus.SetOutput(os.Stdout)

	// Set log level (for prod INFO)
	logrus.SetLevel(logrus.DebugLevel)
}
