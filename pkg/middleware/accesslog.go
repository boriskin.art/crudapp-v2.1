package middleware

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

// AccessLogMiddleware log every request
func AccessLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		next.ServeHTTP(w, r)

		logrus.Info("New request ",
			"method ", r.Method,
			" remote_addr ", r.RemoteAddr,
			" host ", r.Host,
			" url ", r.URL.Path,
			" time ", time.Since(start),
		)
	})
}
