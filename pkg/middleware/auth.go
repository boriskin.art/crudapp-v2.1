package middleware

import (
	"context"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	"net/http"
	"strings"
)

var withoutAuthPaths = map[string]struct{}{
	"/api/v1/sign-in": {},
	"/api/v1/sign-up": {},
	"/docs/swagger":   {},
	"/favicon.ico":    {},
}

// AuthMiddleware checks user auth
func AuthMiddleware(uu domain.UserUsecase, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// New type alias to string for context with value
		type UserID string

		// Get token from Header (name: Authorization)
		token := r.Header.Get("Authorization")

		// Validate token and get userID from JWT Payload
		userID, _ := uu.CheckJWTToken(token)

		// Check accessibility to url without authorization
		var ok bool
		for url := range withoutAuthPaths {
			if strings.HasPrefix(r.URL.Path, url) {
				ok = true
				break
			}
		}
		// If returned userID = 0, token invalid
		if !ok && userID == 0 {
			domain.ErrResponse(w, domain.ErrUnauthorized, 10)
			return
		}

		if ok && userID != 0 {
			domain.ErrResponse(w, domain.ErrAlreadyAuthorized, 11)
			return
		}
		// Save userID to context
		ctx := context.WithValue(context.Background(), "userID", userID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
