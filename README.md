# О проекте
**Это вторая из трех версий проекта.**

**V1** - [Ссылка](http://217.107.219.38/) / [GitLab](https://gitlab.com/boriskin.art/crudapp)

**V2** - [Ссылка](http://77.222.54.181/) / [GitLab](https://gitlab.com/boriskin.art/crudapp-v2.1)

Реализованы 2 основные части - Auth (включая регистрацию, авторизацию и пользовательские сессии), [аналог bit.ly](http://77.222.54.181//shortit)

Отказался от реализации PasteIt из-за большой схожести с ShortIt, решил двигаться дальше и изучать микросервисы.

В этой версии проект является реализацией REST API.

Архитектура — монолит.

## Документация по API - [SWAGGER](http://77.222.54.181/docs/swagger/index.html)


# Что под капотом?
**Фреймворки не использованы, чтобы понять, как все работает «под капотом»**

1. Роутер - `Gorilla Mux`


2. База данных - `PostgreSQL` (docker-compose), драйвер `pgx` потому что модно, а еще быстрее `lib/pq` на 63%


3. Сессии - `Stateless`, JWT токены, решил попробовать что-то новое, поэтому в отличие от первой версии проекта тут токены


4. Хеширование паролей - `PBKDF2` с солью (генерирую рандомную и сохраняю рядом с паролем)


5. Логирование - `Logrus` - Uber Zap понравился, но хотелось попробовать что-то новое.


6. Configs - `Viper` и `yaml файл`


7. Тестирование - `mockery` - [ссылка](https://github.com/vektra/mockery)

# С какими проблемами столкнулся?
1. Сложность с реализацией Gitlab CI (достаточно мало документации на русском именно для golang, зато подучил английский)


# Что можно улучшить?
1. Разбить монолит на микросервисы (будет реализовано в V3)

2. Онлайн configs `Consul` или `Vault` (будет реализовано в V3)

3. Реализовать `CD` подход через `GitLab`

4. Необходимо изучить системы сбора логов и мониторинга

5. Хочу попробовать нагрузочное тестирование


# Чуть-чуть обо мне

Привет! Меня зовут Борискин Артём, а это первая версия моего проекта

Я хотел показать всё, на что способен, надеюсь у меня получилось достойно

Программирую с 14 лет в качестве хобби, начинал с Java, потом долгое время учился на Python

Последний год занимался репетиторством на Python, увлекал детей программированием :)

Сейчас изучаю Golang

...


