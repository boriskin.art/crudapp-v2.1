package link

import (
	"encoding/json"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	"net/http"
)

// CreateLinkInput struct, that we pull from user
type CreateLinkInput struct {
	LongLink string `json:"long_link" valid:"url,required"`
}

// CreateLinkResponse struct, that we returns to user
type CreateLinkResponse struct {
	Code    int    `json:"status_code"`
	Token   string `json:"token"`
}

// NewLinkCreateResponse encode response to json and send to user
func NewLinkCreateResponse(w http.ResponseWriter, token string) {
	w.Header().Add("Access-Control-Allow-Headers", "Authorization")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(CreateLinkResponse{
		Code:    0,
		Token:   token,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


// GetLongLinkInput struct, that we pull from user
type GetLongLinkInput struct {
	Token string `json:"token" valid:"required"`
}

// GetLongLinkResponse struct, that we returns to user
type GetLongLinkResponse struct {
	Code     int    `json:"status_code"`
	LongLink string `json:"long_link"`
}

// NewGetLongLinkResponse encode response to json and send to user
func NewGetLongLinkResponse(w http.ResponseWriter, longLink string) {
	w.Header().Add("Access-Control-Allow-Headers", "Authorization")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(GetLongLinkResponse{
		Code:     0,
		LongLink: longLink,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


// GetLinksForUserInput struct, that we pull from user
type GetLinksForUserInput struct {
	UserID int `json:"user_id" valid:"int,required"`
}

// GetLinksForUserResponse struct, that we return to user
type GetLinksForUserResponse struct {
	Code  int            `json:"status_code"`
	Links []*domain.Link `json:"links"`
}

// NewGetLinksForUserResponse encode response to json and send to user
func NewGetLinksForUserResponse(w http.ResponseWriter, links []*domain.Link) {
	w.Header().Add("Access-Control-Allow-Headers", "Authorization")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(GetLinksForUserResponse{
		Code:  0,
		Links: links,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
