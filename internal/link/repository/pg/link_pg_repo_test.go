package pg

import (
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	"testing"
)

// TestLinkPgRepository_Create test Create method of link repo
func TestLinkPgRepository_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	link := &domain.Link{
		ID:          1,
		UserID:      1,
		Token:       "123456",
		LongLink:    "test.ru",
		ClicksCount: 0,
	}

	repo := NewLinkPgRepository(db)

	rows := sqlmock.NewRows([]string{"id"}).
		AddRow(link.ID)

	// good query
	mock.ExpectQuery("INSERT").
		WithArgs(link.UserID, link.Token, link.LongLink).
		WillReturnRows(rows)

	id, err := repo.Create(link)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, link.ID, id)

	// db_error
	mock.ExpectQuery("INSERT").
		WithArgs(link.UserID, link.Token, link.LongLink).
		WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.Create(link)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, id)

}

// TestLinkPgRepository_GetByToken test GetByToken method of link repo
func TestLinkPgRepository_GetByToken(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	var token = "123456"

	expect := &domain.Link{
		ID:          1,
		UserID:      1,
		Token:       token,
		LongLink:    "test.ru",
		ClicksCount: 0,
	}

	repo := NewLinkPgRepository(db)

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "long_link", "clicks_count"}).
		AddRow(expect.ID, expect.UserID, expect.Token, expect.LongLink, expect.ClicksCount)

	//  good query
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnRows(rows)

	link, err := repo.GetByToken(token)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expect, link)

	// db_error
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnError(fmt.Errorf("db_error"))

	link, err = repo.GetByToken(token)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, link)
}

// TestLinkPgRepository_GetByLongLinkAndUserID test GetByLongLinkAndUserID method of link repo
func TestLinkPgRepository_GetByLongLinkAndUserID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	var longLink = "test.ru"
	var userID = 1

	expect := &domain.Link{
		ID:          userID,
		UserID:      1,
		Token:       "123456",
		LongLink:    longLink,
		ClicksCount: 0,
	}

	repo := NewLinkPgRepository(db)

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "long_link", "clicks_count"}).
		AddRow(expect.ID, expect.UserID, expect.Token, expect.LongLink, expect.ClicksCount)

	//  good query
	mock.ExpectQuery("SELECT").
		WithArgs(longLink, userID).
		WillReturnRows(rows)

	link, err := repo.GetByLongLinkAndUserID(longLink, userID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expect, link)

	// db_error
	mock.ExpectQuery("SELECT").
		WithArgs(longLink, userID).
		WillReturnError(fmt.Errorf("db_error"))

	link, err = repo.GetByLongLinkAndUserID(longLink, userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, link)
}

// TestLinkPgRepository_GetByUserID test GetByUserID method of link repo
func TestLinkPgRepository_GetByUserID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewLinkPgRepository(db)

	var userID = 1

	expect := []*domain.Link{
		{ID: 1, UserID: userID, Token: "123456", LongLink: "test1.ru", ClicksCount: 1},
		{ID: 2, UserID: userID, Token: "654321", LongLink: "test2.ru", ClicksCount: 10},
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "long_link", "clicks_count"})

	for _, link := range expect {
		rows.AddRow(link.ID, link.UserID, link.Token, link.LongLink, link.ClicksCount)
	}

	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	links, err := repo.GetByUserID(userID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expect, links)

	// bad query
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnError(fmt.Errorf("db_error"))

	links, err = repo.GetByUserID(userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, links)

	// Scan error
	rows = sqlmock.NewRows([]string{"id"}).AddRow(expect[0].ID)
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	links, err = repo.GetByUserID(userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, links)
}

// TestLinkPgRepository_IncrementClicksCount test IncrementClicksCount method of link repo
func TestLinkPgRepository_IncrementClicksCount(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := NewLinkPgRepository(db)

	var token = "123456"

	// good query
	mock.ExpectExec("UPDATE").
		WithArgs(token).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = repo.IncrementClicksCount(token)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// db_error
	mock.ExpectExec("UPDATE").
		WithArgs(token).
		WillReturnError(fmt.Errorf("db_error"))

	err = repo.IncrementClicksCount(token)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}
