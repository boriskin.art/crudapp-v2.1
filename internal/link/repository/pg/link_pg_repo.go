package pg

import (
	"database/sql"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
)

// LinkPgRepository implementation of LinkRepository for postgres
type LinkPgRepository struct {
	DB *sql.DB
}

// NewLinkPgRepository initialize new postgres link repository
func NewLinkPgRepository(db *sql.DB) *LinkPgRepository {
	return &LinkPgRepository{
		DB: db,
	}
}

// Create get link object and save it to db
func (lr *LinkPgRepository) Create(link *domain.Link) (int, error) {
	var id int
	err := lr.DB.QueryRow(
		"INSERT INTO link (user_id, token, long_link, clicks_count) VALUES ($1, $2, $3, 0) RETURNING id;",
		link.UserID, link.Token, link.LongLink).
		Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

// GetByToken returns link object from db by token
func (lr *LinkPgRepository) GetByToken(token string) (*domain.Link, error) {
	link := &domain.Link{}

	err := lr.DB.QueryRow(
		"SELECT id, user_id, token, long_link, clicks_count FROM link WHERE token=$1", token).
		Scan(&link.ID, &link.UserID, &link.Token, &link.LongLink, &link.ClicksCount)

	if err != nil {
		return nil, err
	}

	return link, nil
}

// GetByUserID returns link object from db by userID
func (lr *LinkPgRepository) GetByUserID(userID int) ([]*domain.Link, error) {
	var links []*domain.Link
	rows, err := lr.DB.Query(
		"SELECT id, user_id, token, long_link, clicks_count FROM link WHERE user_id=$1", userID)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		l := &domain.Link{}
		err := rows.Scan(&l.ID, &l.UserID, &l.Token, &l.LongLink, &l.ClicksCount)
		if err != nil {
			return nil, err
		}

		links = append(links, l)
	}

	return links, nil
}

// GetByLongLinkAndUserID returns link object by longLink and userID, that is we check whether the
// user already has such link
func (lr *LinkPgRepository) GetByLongLinkAndUserID(longLink string, userID int) (*domain.Link, error) {
	link := &domain.Link{}

	err := lr.DB.QueryRow(
		"SELECT id, user_id, token, long_link, clicks_count FROM link WHERE long_link=$1 and user_id=$2",
		longLink, userID).
		Scan(&link.ID, &link.UserID, &link.Token, &link.LongLink, &link.ClicksCount)

	if err != nil {
		return nil, err
	}

	return link, nil
}

// IncrementClicksCount increment clicks_count field
func (lr *LinkPgRepository) IncrementClicksCount(token string) error {
	_, err := lr.DB.Exec(
		"UPDATE link SET clicks_count = clicks_count + 1 WHERE token=$1", token)
	if err != nil {
		return err
	}

	return nil
}
