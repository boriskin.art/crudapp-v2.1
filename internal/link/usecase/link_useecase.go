package usecase

import (
	"github.com/asaskevich/govalidator"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
)

// LinkUsecase implementation of domain.LinkUsecase, binds usecase and repository layers
type LinkUsecase struct {
	LinkRepository domain.LinkRepository
}

// NewLinkUsecase initialize new link usecase
func NewLinkUsecase(r domain.LinkRepository) *LinkUsecase {
	return &LinkUsecase{
		LinkRepository: r,
	}
}

// Create method, that is wrapper over the repository.Create method
func (lu *LinkUsecase) Create(link *domain.Link) (*domain.Link, error) {
	_, err := govalidator.ValidateStruct(link)
	if err != nil {
		return nil, err
	}

	_, err = lu.LinkRepository.Create(link)
	if err != nil {
		return nil, err
	}

	return link, nil
}

// GetByToken method, that is wrapper over the repository.GetByToken method
func (lu *LinkUsecase) GetByToken(token string) (*domain.Link, error) {
	link, err := lu.LinkRepository.GetByToken(token)
	if err != nil {
		return nil, err
	}

	return link, nil
}

// CheckExistence method, that is wrapper over the repository.GetByLongLinkAndUserID method
func (lu *LinkUsecase) CheckExistence(longLink string, userID int) (bool, *domain.Link, error) {
	link, err := lu.LinkRepository.GetByLongLinkAndUserID(longLink, userID)
	if err != nil {
		return false, nil, err
	}

	return true, link, nil
}

// GetByUserID method, that is wrapper over the repository.GetByUserID method
func (lu *LinkUsecase) GetByUserID(userID int) ([]*domain.Link, error) {
	links, err := lu.LinkRepository.GetByUserID(userID)
	if err != nil {
		return nil, err
	}

	return links, nil
}

// Click method, that is wrapper over the repository.IncrementClicksCount method
func (lu *LinkUsecase) Click(token string) error {
	err := lu.LinkRepository.IncrementClicksCount(token)
	if err != nil {
		return err
	}

	return nil
}
