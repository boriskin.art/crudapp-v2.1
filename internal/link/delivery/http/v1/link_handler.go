package v1

import (
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	linkHelper "gitlab.com/boriskin.art/crudapp-v2.1/internal/link"
	"gitlab.com/boriskin.art/crudapp-v2.1/pkg/token"
	"net/http"
)

// LinkV1Handler need to connect usecase and handler level
type LinkV1Handler struct {
	LinkUsecase domain.LinkUsecase
}

// NewLinkV1Handler initialize new link handler and routes for short-it service
func NewLinkV1Handler(router *mux.Router, linkUsecase domain.LinkUsecase) {
	handler := &LinkV1Handler{
		LinkUsecase: linkUsecase,
	}

	// Routes for short-it service
	router.HandleFunc("/short-it", handler.CreateLink).Methods("POST")
	router.HandleFunc("/short-it/link", handler.GetLongLink).Methods("POST")
	router.HandleFunc("/short-it/links", handler.GetLinksForUser).Methods("GET")
}

// CreateLink godoc
// @Summary Create new link by user_id and long link
// @Description Returns token
// @ID create-link
// @Tags Links
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param input body link.CreateLinkInput true "Create link input"
// @Success 200 {object} link.CreateLinkResponse
// @Failure default {object} domain.ErrorResponse
// @Router /short-it [post]
func (lh *LinkV1Handler) CreateLink(w http.ResponseWriter, r *http.Request) {
	link := linkHelper.CreateLinkInput{}

	err := json.NewDecoder(r.Body).Decode(&link)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	_, err = govalidator.ValidateStruct(link)
	if err != nil {
		domain.ErrResponse(w, err, 30)
		return
	}
	userID := r.Context().Value("userID").(int)
	isExist, l, _ := lh.LinkUsecase.CheckExistence(link.LongLink, userID)

	if !isExist {
		l, err = lh.LinkUsecase.Create(&domain.Link{
			UserID:   userID,
			Token:    token.GenerateToken(viper.GetString("short_it.salt")),
			LongLink: link.LongLink,
		})
		if err != nil {
			domain.ErrResponse(w, err, 20)
			return
		}
	}
	linkHelper.NewLinkCreateResponse(w, l.Token)
}

// GetLongLink godoc
// @Summary Get long link by token
// @Description Returns long link
// @ID get-long-link
// @Tags Links
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param input body link.GetLongLinkInput true "Get long link input"
// @Success 200 {object} link.GetLongLinkResponse
// @Failure default {object} domain.ErrorResponse
// @Router /short-it/link [post]
func (lh *LinkV1Handler) GetLongLink(w http.ResponseWriter, r *http.Request) {
	l := linkHelper.GetLongLinkInput{}

	err := json.NewDecoder(r.Body).Decode(&l)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	link, err := lh.LinkUsecase.GetByToken(l.Token)
	if err != nil {
		domain.ErrResponse(w, err, 21)
		return
	}

	linkHelper.NewGetLongLinkResponse(w, link.LongLink)
}

// GetLinksForUser godoc
// @Summary Get all links
// @Description Returns all links
// @ID get-links-for-user
// @Security ApiKeyAuth
// @Tags Links
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} link.GetLinksForUserResponse
// @Failure default {object} domain.ErrorResponse
// @Router /short-it/links [get]
func (lh *LinkV1Handler) GetLinksForUser(w http.ResponseWriter, r *http.Request) {
	links, err := lh.LinkUsecase.GetByUserID(r.Context().Value("userID").(int))
	if err != nil {
		domain.ErrResponse(w, err, 25)
		return
	}

	linkHelper.NewGetLinksForUserResponse(w, links)
}
