package v1

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	userHelper "gitlab.com/boriskin.art/crudapp-v2.1/internal/user"
	"net/http"
)

// UserV1Handler need to connect usecase and handler level
type UserV1Handler struct {
	UserUsecase domain.UserUsecase
}

// NewUserV1Handler initialize new user handler and routes for auth service
func NewUserV1Handler(router *mux.Router, userUsecase domain.UserUsecase) {
	handler := &UserV1Handler{
		UserUsecase: userUsecase,
	}

	// Routes for auth service
	router.HandleFunc("/sign-up", handler.SignUp).Methods("POST")
	router.HandleFunc("/sign-in", handler.SignIn).Methods("POST")

}


// SignUp godoc
// @Summary User Sign Up
// @Description Sign Up to api service, returns auth token
// @ID sign-up
// @Tags Auth
// @Accept json
// @Produce json
// @Param input body user.AuthInput true "Sign Up input"
// @Success 200 {object} user.AuthResponse
// @Failure default {object} domain.ErrorResponse
// @Router /sign-up [post]
func (uh *UserV1Handler) SignUp(w http.ResponseWriter, r *http.Request) {
	user := domain.User{}

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	token, err := uh.UserUsecase.Create(user)
	if err != nil {
		domain.ErrResponse(w, err, 3)
		return
	}

	userHelper.NewUserAuthResponse(w, token)
}

// SignIn godoc
// @Summary User Sign In
// @Description Authorization to api service, returns auth token
// @ID sign-in
// @Tags Auth
// @Accept json
// @Produce json
// @Param input body user.AuthInput true "Sign In input"
// @Success 200 {object} user.AuthResponse
// @Failure default {object} domain.ErrorResponse
// @Router /sign-in [post]
func (uh *UserV1Handler) SignIn(w http.ResponseWriter, r *http.Request) {
	user := domain.User{}

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	u, err := uh.UserUsecase.CheckUser(user)
	if err != nil {
		domain.ErrResponse(w, err, 2)
		return
	}

	token, err := uh.UserUsecase.GenerateJWTToken(u.ID)
	if err != nil {
		domain.ErrResponse(w, err, 4)
		return
	}
	userHelper.NewUserAuthResponse(w, token)
}
