package user

import (
	"encoding/json"
	"net/http"
)

// AuthInput struct, that we pull from user
type AuthInput struct {
	Email     string  `json:"email" binding:"required,email,max=200"`
	Password  string  `json:"password" binding:"required,min=8,max=100"`
}

// AuthResponse struct, that we return to registered user
type AuthResponse struct {
	Code 	   int     `json:"status_code"`
	AuthToken  string  `json:"auth_token"`
}

// NewUserAuthResponse encode response to json and send to user
func NewUserAuthResponse(w http.ResponseWriter, token string) {
	w.Header().Add("Access-Control-Allow-Headers", "Authorization")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	err := json.NewEncoder(w).Encode(AuthResponse{
		Code:      0,
		AuthToken: token,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
