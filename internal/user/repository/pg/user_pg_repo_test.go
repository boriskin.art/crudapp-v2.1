package pg

import (
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	"testing"
)

func TestUserPgRepository_Create(t *testing.T) {
	db, mock, err := sqlmock.New()

	assert.NoError(t, err)

	defer db.Close()

	repo := NewUserPgRepository(db)

	var userID = 1

	rows := sqlmock.NewRows([]string{"id"}).AddRow(userID)

	expect := domain.User{
		Email:    "test@test.ru",
		Password: "test",
	}

	// good query
	mock.ExpectQuery("INSERT INTO users").
		WithArgs(expect.Email, expect.Password).
		WillReturnRows(rows)

	id, err := repo.Create(expect)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 1, id)

	// bad query
	mock.ExpectQuery("INSERT INTO users").
		WithArgs(expect.Email, expect.Password).
		WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.Create(expect)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, id)

	// Scan error
	rows = sqlmock.NewRows([]string{})

	mock.ExpectQuery("INSERT INTO users").
		WithArgs(expect.Email, expect.Password).
		WillReturnRows(rows)

	id, err = repo.Create(expect)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, id)
}

func TestUserPgRepository_GetByEmail(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	user := &domain.User{
		ID:       1,
		Email:    "test@test.ru",
		Password: "test",
	}

	repo := NewUserPgRepository(db)

	rows := sqlmock.NewRows([]string{"id", "email", "password"}).AddRow(user.ID, user.Email, user.Password)

	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(user.Email).
		WillReturnRows(rows)

	u, err := repo.GetByEmail(user.Email)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, u, user)

	// bad query
	mock.ExpectQuery("SELECT").
		WithArgs(user.Email).
		WillReturnError(fmt.Errorf("db_error"))

	u, err = repo.GetByEmail(user.Email)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)

	// user doesn't exist
	rows = sqlmock.NewRows([]string{"id", "email", "password"})
	mock.ExpectQuery("SELECT").
		WithArgs(user.Email).
		WillReturnRows(rows)

	u, err = repo.GetByEmail(user.Email)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)

	// Scan error
	rows = sqlmock.NewRows([]string{"id"}).AddRow(1)

	mock.ExpectQuery("SELECT").
		WithArgs(user.Email).
		WillReturnRows(rows)

	u, err = repo.GetByEmail(user.Email)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)
}

func TestUserPgRepository_GetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	user := &domain.User{
		ID:       1,
		Email:    "test@test.ru",
		Password: "test",
	}

	repo := NewUserPgRepository(db)

	rows := sqlmock.NewRows([]string{"id", "email", "password"}).AddRow(user.ID, user.Email, user.Password)

	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(user.ID).
		WillReturnRows(rows)

	u, err := repo.GetByID(user.ID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, u, user)

	// bad query
	mock.ExpectQuery("SELECT").
		WithArgs(user.ID).
		WillReturnError(fmt.Errorf("db_error"))

	u, err = repo.GetByID(user.ID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)

	// user doesn't exist
	rows = sqlmock.NewRows([]string{"id", "email", "password"})
	mock.ExpectQuery("SELECT").
		WithArgs(user.ID).
		WillReturnRows(rows)

	u, err = repo.GetByID(user.ID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)

	// Scan error
	rows = sqlmock.NewRows([]string{"id"}).AddRow(1)

	mock.ExpectQuery("SELECT").
		WithArgs(user.ID).
		WillReturnRows(rows)

	u, err = repo.GetByID(user.ID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, u)
}
