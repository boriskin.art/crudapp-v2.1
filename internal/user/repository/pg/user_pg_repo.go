package pg

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
)

// UserPgRepository implementation of UserRepository for postgres
type UserPgRepository struct {
	DB *sql.DB
}

// NewUserPgRepository initialize new postgres user repository
func NewUserPgRepository(db *sql.DB) *UserPgRepository {
	return &UserPgRepository{
		DB: db,
	}
}

// Create get user object and save it to db
func (repo *UserPgRepository) Create(user domain.User) (int, error) {
	var id int
	logrus.Debug(user.Email, user.Password)
	err := repo.DB.QueryRow(`INSERT INTO users (email, password) VALUES ($1, $2) RETURNING id;`,
		user.Email,
		user.Password).
		Scan(&id)
	logrus.Debug(err)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// GetByID returns user object from db by userID
func (repo *UserPgRepository) GetByID(userID int) (*domain.User, error) {
	user := &domain.User{}
	logrus.Debug("Get by id")

	err := repo.DB.QueryRow("SELECT id, email, password FROM users WHERE id=$1", userID).
		Scan(&user.ID, &user.Email, &user.Password)
	if err == sql.ErrNoRows {
		return nil, domain.ErrAuthUserNotFound
	}
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetByEmail returns user object from db by email
func (repo *UserPgRepository) GetByEmail(email string) (*domain.User, error) {
	user := &domain.User{}
	logrus.Debug("Get by email")

	err := repo.DB.QueryRow("SELECT id, email, password FROM users WHERE email=$1", email).
		Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		return nil, err
	}

	return user, nil

}
