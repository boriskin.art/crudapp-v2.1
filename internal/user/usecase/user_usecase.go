package usecase

import (
	"crypto/rand"
	"database/sql"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
	"gitlab.com/boriskin.art/crudapp-v2.1/internal/domain"
	"gitlab.com/boriskin.art/crudapp-v2.1/pkg/passwords"
	"strconv"
	"time"
)

// UserUsecase implementation of domain.UserUsecase, binds usecase and repository layers
type UserUsecase struct {
	UserRepository domain.UserRepository
}

// NewUserUsecase initialize new user usecase
func NewUserUsecase(ur domain.UserRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository: ur,
	}
}

// Create method, that is wrapper over the repository.Create method
func (uu *UserUsecase) Create(user domain.User) (string, error) {
	_, err := govalidator.ValidateStruct(user)
	if err != nil {
		return "", err
	}

	isExist, err := uu.CheckUserExistence(user.Email)
	if err != nil {
		return "", err
	}
	if isExist {
		return "", domain.ErrAuthUserAlreadyExist
	}

	salt := make([]byte, 8)
	rand.Read(salt)
	user.Password = passwords.HashPassword(fmt.Sprintf("%x", salt), user.Password)

	id, err := uu.UserRepository.Create(user)
	if err != nil {
		return "", err
	}

	token, err := uu.GenerateJWTToken(id)
	if err != nil {
		return "", nil
	}

	return token, nil
}

// CheckUserExistence method, that is wrapper over the repository.GetByEmail method
func (uu *UserUsecase) CheckUserExistence(email string) (bool, error) {
	_, err := uu.UserRepository.GetByEmail(email)

	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

// CheckUser method, that is wrapper over the repository.GetByEmail method
func (uu *UserUsecase) CheckUser(user domain.User) (*domain.User, error) {
	_, err := govalidator.ValidateStruct(user)
	if err != nil {
		return nil, err
	}

	u := &domain.User{}
	u, err = uu.UserRepository.GetByEmail(user.Email)
	if err == sql.ErrNoRows {
		return nil, domain.ErrAuthUserNotFound
	}
	if err != nil {
		return nil, err
	}

	if !passwords.CheckPassword(u.Password, user.Password) {
		return nil, domain.ErrAuthUserBadPassword
	}
	return u, nil
}

// GenerateJWTToken method, that returns prepared JWT token
func (uu *UserUsecase) GenerateJWTToken(userID int) (string, error) {
	// Create a new token object
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
		Subject:   strconv.Itoa(userID),
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte(viper.GetString("jwt_secret")))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// CheckJWTToken method, that validate JWT token and returns userID
func (uu *UserUsecase) CheckJWTToken(token string) (int, error) {
	t, err := jwt.Parse(token, func(token *jwt.Token) (i interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(viper.GetString("jwt_secret")), nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		return 0, fmt.Errorf("error get user claims from token")
	}

	userID, _ := strconv.Atoi(claims["sub"].(string))
	return userID, nil
}
