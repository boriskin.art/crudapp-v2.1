package crudapp

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/boriskin.art/crudapp-v2.1/configs"
	"gitlab.com/boriskin.art/crudapp-v2.1/docs"
	linkV1 "gitlab.com/boriskin.art/crudapp-v2.1/internal/link/delivery/http/v1"
	linkPg "gitlab.com/boriskin.art/crudapp-v2.1/internal/link/repository/pg"
	linkUC "gitlab.com/boriskin.art/crudapp-v2.1/internal/link/usecase"
	userV1 "gitlab.com/boriskin.art/crudapp-v2.1/internal/user/delivery/http/v1"
	userPg "gitlab.com/boriskin.art/crudapp-v2.1/internal/user/repository/pg"
	userUC "gitlab.com/boriskin.art/crudapp-v2.1/internal/user/usecase"
	"gitlab.com/boriskin.art/crudapp-v2.1/pkg/dbconnections"
	"gitlab.com/boriskin.art/crudapp-v2.1/pkg/logger"
	"gitlab.com/boriskin.art/crudapp-v2.1/pkg/middleware"
	"net/http"
	"os"
	"time"
)

// Run godoc
// @title CRUD App API
// @version 2.1
// @description This is a tutorial project built in golang
// @contact.name API Support
// @contact.email boriskin.art@yandex.ru
// @host localhost:8080
// @BasePath /api/v1/
// @schemes http
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func Run() {
	// Initialize configs
	configs.InitConfig()

	// Initialize logger (logrus)
	logger.InitLogger()

	// Change swagger host
	if host := os.Getenv("CRUDAPP_DOMAIN"); host != "" {
		docs.SwaggerInfo.Host = host
		docs.SwaggerInfo.Schemes = []string{"http", "https"}
	}

	// Initialize main router
	router := mux.NewRouter()
	router.PathPrefix("/docs").Handler(httpSwagger.WrapHandler)

	// API Sub-router
	apiV1Router := router.PathPrefix("/api/v1").Subrouter()

	// User initialization part
	userRepository := userPg.NewUserPgRepository(dbconnections.PostgresConnection())
	userUsecase := userUC.NewUserUsecase(userRepository)
	userV1.NewUserV1Handler(apiV1Router, userUsecase)

	// Link initialization part
	linkRepository := linkPg.NewLinkPgRepository(dbconnections.PostgresConnection())
	linkUsecase := linkUC.NewLinkUsecase(linkRepository)
	linkV1.NewLinkV1Handler(apiV1Router, linkUsecase)

	// Middlewares
	m := middleware.AuthMiddleware(userUsecase, router)
	m = middleware.AccessLogMiddleware(m)
	m = middleware.PanicMiddleware(m)

	srv := &http.Server{
		Addr:         viper.GetString("port"),
		Handler:      m,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logrus.Info("Starting server at http://" + viper.GetString("domain"))

	if err := srv.ListenAndServe(); err != nil {
		logrus.Fatal("Server error:", err)
	}
}
