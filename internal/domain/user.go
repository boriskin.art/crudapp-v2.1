package domain

// User entity struct
type User struct {
	ID       int    `json:"id" valid:"optional"`
	Email    string `json:"email" valid:"email,required"`
	Password string `json:"password" valid:"required"`
}

// UserUsecase interface
type UserUsecase interface {
	Create(user User) (string, error)
	CheckUser(user User) (*User, error)
	CheckUserExistence(email string) (bool, error)
	GenerateJWTToken(userID int) (string, error)
	CheckJWTToken(token string) (int, error)
}

// Generate mocks
// mockery --name=UserRepository

// UserRepository interface
type UserRepository interface {
	Create(user User) (int, error)
	GetByID(userID int) (*User, error)
	GetByEmail(email string) (*User, error)
}
