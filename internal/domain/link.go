package domain

// Link entity struct
type Link struct {
	ID          int    `json:"id" valid:"optional"`
	UserID      int    `json:"user_id" valid:"int,required"`
	Token       string `json:"token" valid:"optional"`
	LongLink    string `json:"long_link" valid:"url,required"`
	ClicksCount int    `json:"clicks_count" valid:"optional"`
}

// LinkUsecase interface
type LinkUsecase interface {
	Create(link *Link) (*Link, error)
	GetByToken(token string) (*Link, error)
	CheckExistence(longLink string, userID int) (bool, *Link, error)
	GetByUserID(userID int) ([]*Link, error)
	Click(token string) error
}

// Generate mocks
// mockery --name=LinkRepository

// LinkRepository interface
type LinkRepository interface {
	Create(link *Link) (int, error)
	GetByToken(token string) (*Link, error)
	GetByUserID(userID int) ([]*Link, error)
	GetByLongLinkAndUserID(longLink string, userID int) (*Link, error)
	IncrementClicksCount(token string) error
}
