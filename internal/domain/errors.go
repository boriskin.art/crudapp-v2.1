package domain

import (
	"encoding/json"
	"errors"
	"net/http"
)

var (
	// ErrAuthUserNotFound needed to unify the error at all levels
	ErrAuthUserNotFound = errors.New("user doesn't exist")
	// ErrAuthUserBadPassword needed to unify the error at all levels
	ErrAuthUserBadPassword = errors.New("incorrect user password")
	// ErrAuthUserAlreadyExist needed to unify the error at all levels
	ErrAuthUserAlreadyExist = errors.New("user already exist")

	// ErrUnauthorized needed to unify the error at all levels
	ErrUnauthorized = errors.New("need to authorize to access this page")
	// ErrAlreadyAuthorized needed to unify the error at all levels
	ErrAlreadyAuthorized = errors.New("user already authorized")
)

// ErrorResponse struct for ErrResponse
type ErrorResponse struct {
	Code  int    `json:"error_code"`
	Error string `json:"error_message"`
}

// ErrResponse generate and push json error message
func ErrResponse(w http.ResponseWriter, error error, code int) {
	w.Header().Add("Access-Control-Allow-Headers", "Authorization")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	// Encode Response struct to json
	err := json.NewEncoder(w).Encode(ErrorResponse{
		Error: error.Error(),
		Code:  code,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
