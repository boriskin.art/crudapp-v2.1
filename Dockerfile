# Build binary with downloaded go modules
FROM golang:1.16.7-buster as build

# Copy files
RUN mkdir /home/crudapp-v2
COPY ./go.mod /home/crudapp-v2
WORKDIR /home/crudapp-v2

# Download go modules to cache
RUN go mod download
COPY . /home/crudapp-v2

# Build application
RUN CGO_ENABLED=0 go build -o crudapp ./cmd/crudapp/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir crudapp-v2

# Copy files from build step
WORKDIR crudapp-v2
COPY --from=build /home/crudapp-v2/crudapp .
RUN mkdir configs
COPY --from=build /home/crudapp-v2/configs ./configs

# Start app
CMD ["./crudapp"]

